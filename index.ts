import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import * as synced from "@pulumi/synced-folder";

//---------------------nombres-----------------------------
const nameApi = "prueba9-backend"
const rootApi = "prueba9-backend"


//------------------------------bucket---------------------------------------------------

const bucket = new aws.s3.Bucket("buckect", {
    versioning: { enabled: true }, //habilita la opciono de generar un versionado del bucket
    website: { indexDocument: "index.html" }, // Indica que la sito web tomara el index.html 
});

//controla que los objetos se pudan escribir a traves de una regla
const controls = new aws.s3.BucketOwnershipControls(`bucketControls`, {
    bucket: bucket.id,
    rule: { objectOwnership: "ObjectWriter" },
});

// Quita el bloqueo al publico que vine activado de forma automatica
const bloqueo = new aws.s3.BucketPublicAccessBlock(`bucketBloqueo`, {
    bucket: bucket.id,
    blockPublicAcls: false,
    blockPublicPolicy: false,
    ignorePublicAcls: false,
    restrictPublicBuckets: false,
});

const objetos = new synced.S3BucketFolder(`bucketObj`, {
    path: `./app`, //el path de la carpta que deseamos exportar
    bucketName: bucket.bucket,
    acl: aws.s3.PublicReadAcl,
}, { dependsOn: [bloqueo, controls] }); //esto indica que el comando funcionará despues de que esos comandos ya se ejecutarón

const websiteUrl = pulumi.interpolate`http://${bucket.bucket}.s3-website-${aws.config.region}.amazonaws.com`;
export const nameBucket = bucket.id;
//----------------------------------------dynamon table----------------------------------------------------

const dynamoDBTable = new aws.dynamodb.Table(`${nameApi}-table`, {
    name: `${nameApi}-table`,
    attributes: [
        { name: "id", type: "S", }, //Tipo de atributos son `S` (string), `N` (number), `B` (binary).
        { name: "name", type: "S", },
    ],
    billingMode: "PROVISIONED",
    hashKey: "id", // Este será el atributo obligatorio que se deberá proporcionar
    rangeKey:"name",
    readCapacity: 5,
    writeCapacity: 5,
});
export const dynamoDBTable1 = dynamoDBTable.name;
//---------------------------------------Lambda-------------------------------------------------------------

// Creación de una políticapara el servicio lambda
const policyLambda = aws.iam.getPolicyDocument({
    statements: [{
        effect: "Allow",
        principals: [{
            type: "Service",
            identifiers: ["lambda.amazonaws.com"],
        }],
        actions: ["sts:AssumeRole"],
    }]
});

// Combina las políticas en un documento de política principal
const iamForLambda = new aws.iam.Role("roleForLambda", { assumeRolePolicy: policyLambda.then(assumeRole => assumeRole.json) });

//Creación de la función de lambda publicando el comando predeterminado
const lambdaFunction = new aws.lambda.Function(`funtion${nameApi}`, {
    runtime: "nodejs18.x", //indica el idioma y version que se va útilizar
    role: iamForLambda.arn, // El rol que se va a usar
    handler: "index.handler", // indica que archivo se va usar
    code: new pulumi.asset.FileArchive("./index.zip"), //exportará a la función el archivo .zip antes creado
});

//Exporta el nombre del Lambda que se va a usar
export const nameFunctionLambda = lambdaFunction.name;

//---------------------------------Api-Gateway-------------------------------------------

//Creación de un nuevo api gatewey tipo REST este se creará despues de la función lambda

const apiName = `${nameApi}-api`; // Nombre personalizado para tu API Gateway

const apiJava = new aws.apigateway.RestApi(apiName, {}, { dependsOn: [lambdaFunction] });

//Se crea un recurso en el api gatewey
const resourceApi = new aws.apigateway.Resource(`recurso`, {
    parentId: apiJava.rootResourceId,
    pathPart: "default",// Nombre de la ruta en donde se guardará el recurso
    restApi: apiJava.id,
});

//Se crea la ruta predeterminada
const routeApi = new aws.apigateway.Method(`ruta`, {
    resourceId: resourceApi.id,
    restApi: apiJava.id,
    httpMethod: "POST", // El indicador POST nos permite la comunicación del api y la función
    authorization: "NONE",
});

//integrar la función lambda en el apigateway
const lambdaIntegration = new aws.apigateway.Integration(`integration`, {
    resourceId: resourceApi.id,
    restApi: apiJava.id,
    type: "AWS_PROXY",
    httpMethod: "POST",
    integrationHttpMethod: "POST",
    uri: lambdaFunction.invokeArn,
    timeoutMilliseconds: 3000,
});

const deployment = new aws.apigateway.Deployment("mi-deployment", {
    restApi: apiJava.id,
    // Puedes proporcionar opciones adicionales aquí, si es necesario
});

const stage = new aws.apigateway.Stage(`mi etapa`, {
    restApi: apiJava.id,
    stageName: "stage",
    deployment: deployment.id,
});

//expotar al url del api gateway
export const apiUrl = pulumi.interpolate`https://${apiJava.id}.execute-api.${aws.config.region}.amazonaws.com/${stage.stageName}/default`;

//--------------------------------------------------Cognito------------------------------------

// // Obtener un grupo de ususario creado desde la consola
const userAwsCogn = aws.cognito.getUserPools({
    name: "USER25", // Aquí debes proporcionar el nombre de tu grupo de usuarios creado en Cognito
});

// Agregar una autorización al api
const cognito = new aws.apigateway.Authorizer("cognito", {
    type: "COGNITO_USER_POOLS",//indica el tipo del autorizaor
    restApi: apiJava.id, // indica a cual api Gateway tipo rest irá diriguido
    providerArns: userAwsCogn.then(userAwsCogn => userAwsCogn.arns), //Extrae el arn del user pool
});

// //
export const idApi = apiJava.id;
